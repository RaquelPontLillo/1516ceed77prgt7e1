package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.Funciones;

/**
 * Fichero: ModeloFichero.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 08/02/2016
 */

public class ModeloFichero implements IModelo {  
    public static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private Funciones f = new Funciones();
    File grupos = null; //creamos un objeto de tipo file
    File alumnos = null; //creamos un objeto de tipo file
    
    public ModeloFichero() {
        grupos = new File("FicheroGrupos.csv"); //creamos el archivo para guardar los grupos
        alumnos = new File("FicheroAlumnos.csv"); //creamos el archivo para guardar los alumnos
    }
    
    @Override
    public void create(Alumno alumno) {
        try {
            FileWriter filewriter = new FileWriter(alumnos, true); //creamos un objeto filewriter para utilizarlo sobre el fichero de alumnos
            filewriter.write(alumno.getId() + ";" + alumno.getNombre() + ";" + alumno.getEdad() + ";" + alumno.getEmail() + ";" + alumno.getGrupo().getId() 
                + ";" + SHORT_DATE_FORMAT.format(alumno.getFecha()) + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
            filewriter.close(); //cerramos el fichero de alumnos
        } catch (IOException e) { //capturamos la excepción 
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, e); //mostramos el error
        }
    }
    
    @Override
    public void create(Grupo grupo) {
        try {
            FileWriter filewriter = new FileWriter(grupos, true); //creamos un objeto filewriter para utilizarlo sobre el fichero de grupos
            filewriter.write(grupo.getId() + ";" + grupo.getNombre() + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
            filewriter.close(); //cerramos el fichero de alumnos
        } catch (IOException ex) { //capturamos la excepción 
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex); //mostramos el error
        }
    }
    
    @Override
    public ArrayList<Alumno> readAlumno() {
        ArrayList hashset = new ArrayList();
        try {
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                String idGrupo = stokenizer.nextToken();
                Grupo grupo = new Grupo (idGrupo, null);
                Date fecha = null;
                try {
                    fecha = SHORT_DATE_FORMAT.parse(stokenizer.nextToken());                
                } catch (ParseException pe) {
                    f.error("No se ha podido convertir el campo Date a String.");
                }
                Alumno alumno = new Alumno(idAlumno, nAlumno, edad, email, grupo, fecha);
                hashset.add(alumno);
                string = bufferedreader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashset;
    }
    
    @Override
    public ArrayList<Grupo> readGrupo() {
        ArrayList hashset = new ArrayList();
        try {
            FileReader filereader = new FileReader(grupos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idGrupo = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();
                Grupo grupo = new Grupo(idGrupo, nombre);
                hashset.add(grupo);
                string = bufferedreader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashset;
    }

    @Override
    public void update(Alumno alumno) {
        File temp = new File("temp.csv");
        Alumno a;
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                String idGrupo = stokenizer.nextToken();
                Grupo grupo = new Grupo (idGrupo, null);
                Date fecha = null;
                try {
                    fecha = SHORT_DATE_FORMAT.parse(stokenizer.nextToken());                
                } catch (ParseException pe) {
                    f.error("No se ha podido convertir el campo Date a String.");
                }
                a = new Alumno(idAlumno, nAlumno, edad, email, grupo, fecha);
                if (alumno.getId().equals(a.getId())) {
                    filewriter.write(alumno.getId() + ";" + alumno.getNombre() + ";" + alumno.getEdad() + ";" + alumno.getEmail() 
                            + ";" + alumno.getGrupo().getId() + ";" + SHORT_DATE_FORMAT.format(alumno.getFecha()) + ";\r\n"); 
                } else {
                    filewriter.write(a.getId() + ";" + a.getNombre() + ";" + a.getEdad() + ";" + a.getEmail() + ";" + a.getGrupo().getId() 
                            + ";" + SHORT_DATE_FORMAT.format(a.getFecha()) + ";\r\n");
                }
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        alumnos.delete();
        boolean rename = temp.renameTo(new File("FicheroAlumnos.csv"));
    }
    
    @Override
    public void update(Grupo grupo) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(grupos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idGrupo = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();            
                Grupo g = new Grupo(idGrupo, nombre);
                if (grupo.getId().equals(g.getId())) {
                    filewriter.write(grupo.getId() + ";" + grupo.getNombre() + ";\r\n");
                } else {
                    filewriter.write(g.getId() + ";" + g.getNombre() + ";\r\n");
                }
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        grupos.delete();
        boolean rename = temp.renameTo(new File("FicheroGrupos.csv"));
    }

    @Override
    public void delete(Alumno alumno) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                String idGrupo = stokenizer.nextToken();
                Grupo grupo = new Grupo (idGrupo, null);
                Date fecha = null;
                try {
                    fecha = SHORT_DATE_FORMAT.parse(stokenizer.nextToken());                
                } catch (ParseException pe) {
                    f.error("No se ha podido convertir el campo Date a String.");
                }
                Alumno a = new Alumno(idAlumno, nAlumno, edad, email, grupo, fecha);
                if (!alumno.getId().equals(a.getId())) {
                    filewriter.write(a.getId() + ";" + a.getNombre() + ";" + a.getEdad() + ";" + a.getEmail() + ";" + a.getGrupo().getId() + ";" + SHORT_DATE_FORMAT.format(a.getFecha()) + ";\r\n");
                } 
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        alumnos.delete();
        boolean rename = temp.renameTo(new File("FicheroAlumnos.csv"));
    }

    @Override
    public void delete(Grupo grupo) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(grupos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idGrupo = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();            
                Grupo g = new Grupo(idGrupo, nombre);
                if (!grupo.getId().equals(g.getId())) {
                    filewriter.write(g.getId() + ";" + g.getNombre() + ";\r\n");
                } 
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        grupos.delete();
        boolean rename = temp.renameTo(new File("FicheroGrupos.csv"));
    }
}