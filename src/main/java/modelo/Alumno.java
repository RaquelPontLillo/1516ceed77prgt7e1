package modelo;

import java.util.Date;

/**
 * Fichero: Alumno.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 30/11/2015
 */

public class Alumno {
    /*
    * La clase alumno proporciona los constructores
    * para crear objetos de tipo Alumno y los métodos
    * para recuperar el valor de los atributos de esos 
    * objetos o para darles un nuevo valor.
    */

    //Variables de la clase
    private String id;
    private String nombre;
    private int edad;
    private String email;
    private Grupo grupo;
    private Date fecha;
    
    //Constructores de la clase
    public Alumno() {
    } 
    
    public Alumno(String id, String nombre, int edad, String email, Grupo grupo, Date fecha) {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.email = email;
        this.grupo = grupo;
        this.fecha = fecha;
    }
    
    //Getters
    /*
    * @return el id del alumno
    */
    public String getId() {
        return id;     
    }
    
    /*
    * @return el nombre del alumno
    */
    public String getNombre() {
        return nombre;     
    }
    
    /*
    * @return la edad del alumno
    */
    public int getEdad() {
        return edad;     
    }
    
    /*
    * @return el nombre del alumno
    */
    public String getEmail() {
        return email;     
    }
    
    /*
    * @return el grupo del alumno
    */
    public Grupo getGrupo() {
        return grupo;     
    }
    
    public Date getFecha() {
        return fecha;
    }
    
    //Setters
    /*
    * Cambia el id a un alumno
    * @param id, id a cambiar
    */
    public void setId(String id) {
        this.id = id;
    }
    
    /*
    * Cambia el nombre a un alumno
    * @param nombre, nombre a cambiar
    */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /*
    * Cambia la edad a un alumno
    * @param edad, edad a cambiar
    */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    /*
    * Cambia el email a un alumno
    * @param email, email a cambiar
    */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /*
    * Cambia el grupo de un alumno
    * @param grupo, grupo a cambiar
    */
    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}