package modelo;

import java.util.ArrayList;

/**
 * Fichero: IModelo.java
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/01/2016
 */

public interface IModelo {
        //Alumno
	public void create(Alumno alumno);
	public ArrayList<Alumno> readAlumno();
	public void update(Alumno alumno);
	public void delete(Alumno alumno);
	
	//Grupo
	public void create(Grupo grupo);
	public ArrayList<Grupo> readGrupo();
	public void update(Grupo grupo);
	public void delete(Grupo grupo);
}