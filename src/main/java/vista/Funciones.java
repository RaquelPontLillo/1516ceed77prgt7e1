package vista;

import javax.swing.*;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Funciones {
 public JOptionPane error(String s) {
        JOptionPane error = new JOptionPane();
        error.showMessageDialog(null, s, "Error", error.ERROR_MESSAGE);
        return error;
    }
    
    public JOptionPane info(String s) {
        JOptionPane info = new JOptionPane();
        info.showMessageDialog(null, s, "Información", info.INFORMATION_MESSAGE);
        return info;
    }
}  