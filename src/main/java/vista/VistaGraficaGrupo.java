package vista;

import javax.swing.*;

public class VistaGraficaGrupo extends JFrame {
    //Definimos los botones de la GUI
    private JButton create = new JButton();
    private JButton read = new JButton();
    private JButton update = new JButton();
    private JButton delete = new JButton();
    private JButton guardar = new JButton();
    private JButton cancelar = new JButton();
    private JButton salir = new JButton();
    private JButton siguiente = new JButton();
    private JButton anterior = new JButton();
    private JButton primero = new JButton();
    private JButton ultimo = new JButton();
    
    //Definimos las etiquetas de la GUI
    private JLabel idGrupo = new JLabel("ID grupo: ");
    private JLabel grupo = new JLabel("Grupo: ");
    
    //Definimos los campos de texto de la GUI
    private JTextField tidGrupo = new JTextField();
    private JTextField tgrupo = new JTextField();
    
    public VistaGraficaGrupo() {
        //Llamamos al constructor de la clase JFrame
        super("Menú curso");
        
        //Configuramos el comportamiento del aspa y aspecto general de la GUI
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        //Configuramos los distintos componentes de la GUI
        //CRUD
        create.setText("CREATE");
        read.setText("READ");
        update.setText("UPDATE");
        update.setEnabled(false);
        delete.setText("DELETE");
        delete.setEnabled(false);
        //Navegación
        primero.setText("|<");
        primero.setEnabled(false);
        anterior.setText("<");
        anterior.setEnabled(false);
        siguiente.setText(">");
        siguiente.setEnabled(false);
        ultimo.setText(">|");
        ultimo.setEnabled(false);
        //Guardar, cancelar, salir
        guardar.setText("GUARDAR");
        guardar.setEnabled(false);
        cancelar.setText("CANCELAR");
        cancelar.setEnabled(false);
        salir.setText("SALIR");
        //Datos grupo
        tidGrupo.setEnabled(false);
        tgrupo.setEnabled(false);

        //Definimos el aspecto para nuestro diseño y añadimos los botones y paneles
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(guardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(salir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(idGrupo, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(grupo, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGap(42, 42, 42)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(tidGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(tgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(primero, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(anterior, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(siguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(ultimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(create, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(read, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(10, 10, 10)
                            .addComponent(update)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(delete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(primero)
                    .addComponent(anterior)
                    .addComponent(siguiente)
                    .addComponent(ultimo))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(idGrupo)
                    .addComponent(tidGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(grupo)
                    .addComponent(tgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(create)
                    .addComponent(update)
                    .addComponent(delete)
                    .addComponent(read))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guardar)
                    .addComponent(cancelar)
                    .addComponent(salir))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        
        //Hacemos visible la GUI
        setVisible(true);
    }
    
    //Getters
    public JButton getCreate() {
        return create;
    }
    
    public JButton getRead() {
        return read;
    }
    
    public JButton getUpdate() {
        return update;
    }
    
    public JButton getDelete() {
        return delete;
    }
    
    public JButton getPrimero() {
        return primero;
    }
    
    public JButton getAnterior() {
        return anterior;
    }
    
    public JButton getSiguiente() {
        return siguiente;
    }
    
    public JButton getUltimo() {
        return ultimo;
    }
    
    public JButton getGuardar() {
        return guardar;
    }
    
    public JButton getCancelar() {
        return cancelar;
    }
    
    public JButton getSalir() {
        return salir;
    }
    
    public JTextField getTidGrupo() {
        return tidGrupo;
    }
    
    public JTextField getTgrupo() {
        return tgrupo;
    }
}