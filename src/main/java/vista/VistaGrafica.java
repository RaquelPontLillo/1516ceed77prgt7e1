package vista;

import java.awt.*;
import javax.swing.*;

public class VistaGrafica extends JFrame {
     //Definimos los botones de la GUI
    private JButton alumno = new JButton();
    private JButton grupo = new JButton();
    private JButton documentacion = new JButton();
    private JButton acerca = new JButton();
    private JButton salir = new JButton();
    
    //Definimos los paneles de la GUI
    private JPanel superior = new JPanel();
    private JPanel central = new JPanel();
    private JPanel inferior = new JPanel();
    
    //Definimos las etiquetas de la GUI
    private JLabel logo = new JLabel();
    private JLabel info = new JLabel();
    
    public VistaGrafica() {
        //Llamamos al constructor de la clase JFrame
        super("Menú principal");
        
        //Configuramos el tamaño, comportamiento del aspa y aspecto general de la GUI
        setSize(600,350);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Damos valor a los botones de la GUI (icono y tooltip)
        alumno.setText("Alumno");
        grupo.setText("Grupo");
        documentacion.setText("Documentación");
        acerca.setText("Acerca");
        salir.setText("Salir");
        
        //Damos valor a las etiquetas de la GUI
        //logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo_mayus.png")));
        info.setText("Aplicación de ejemplo");
        
        //Definimos los layouts para nuestro diseño y añadimos los botones y paneles
        GridLayout gl = new GridLayout(3, 1, 10, 10);
        setLayout(gl);
        FlowLayout fl = new FlowLayout(FlowLayout.CENTER, 25, 10);
        superior.setLayout(fl);
        superior.add(logo);
        add(superior);
        central.setLayout(fl);
        central.add(alumno);
        central.add(grupo);
        central.add(documentacion);
        central.add(acerca);
        central.add(salir);
        add(central);
        inferior.setLayout(fl);
        inferior.add(info);
        add(inferior);
            
        //Hacemos visible la GUI
        setVisible(true);
    }

    //Getters
    public JButton getAlumno() {
        return alumno;
    }
    
    public JButton getGrupo() {
        return grupo;
    }
    
    public JButton getDoc() {
        return documentacion;
    }
    
    public JButton getAcerca() {
        return acerca;
    }
    
    public JButton getSalir() {
        return salir;
    }
    
    public JLabel getInfo() {
        return info;
    }
}