package vista;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;


/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Verificador extends InputVerifier {
    private String variable;
    private Funciones f = new Funciones();
  
    public Verificador(String var) {
        variable = var;
    }

    private boolean validarEmail(String s) {
        Pattern pattern = Pattern.compile("[\\w\\.\\d]+@\\w+\\.\\w+");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }

    private boolean validarNumero(String texto) {
        boolean valido = true;
        try {
            int numero = Integer.parseInt(texto);
        } catch (Exception e) {
            int numero;
            valido = false;
        }
        return valido;
    }

    public boolean verify(JComponent input) {
        if (input instanceof JTextField) {
            String texto = ((JTextField)input).getText();
            switch (variable) {
            case "tedad": 
                if (!validarNumero(texto)) {
                    f.error("El valor introducido para la edad no es numérico.");
                    return false;
                }
                break;
            case "temail": 
              if (!validarEmail(texto)) {
                  f.error("El e-mail no sigue un formato válido (ej: nombre@empresa.com)");
                  return false;
              }
                break;
            }   
        }
        return true;
    }
}