package vista;

import javax.swing.*;

public class VistaGraficaAcerca extends JFrame {
     //Definimos los botones de la GUI
    //private JButton salirAcerca = new JButton();
    
    //Definimos las etiquetas de la GUI
    private JLabel jLabel1 = new JLabel();
    private JLabel jLabel2 = new JLabel();
    private JLabel jLabel3 = new JLabel();
    private JLabel jLabel4 = new JLabel();
    private JLabel jLabel5 = new JLabel();
    
    public VistaGraficaAcerca() {
        //Llamamos al constructor de la clase JFrame
        super("Acerca de");
        
        //Configuramos el tamaño, comportamiento del aspa y aspecto general de la GUI
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        //Damos valor a los botones de la GUI
        //salirAcerca.setText("Salir");
        
        //Damos valor a las etiquetas de la GUI
        jLabel1.setText("Aplicación de ejemplo");
        jLabel2.setText("13/Mar/2016");
        jLabel3.setText("raquel.pont.lillo@gmail.com");
        jLabel4.setText("DAW 1. Programación");
        jLabel5.setText("Por Raquel Pont Lillo");
        
        //Definimos los layouts para nuestro diseño y añadimos los botones y paneles
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            //    .addComponent(salirAcerca)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(68, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(38, 38, 38)
             //   .addComponent(salirAcerca)
                .addContainerGap())
        );

        pack();
            
        //Hacemos visible la GUI
        setVisible(true);
    }
    
    //Geters
//    public JButton getSalirAcerca() {
//        return salirAcerca;
//    }
}