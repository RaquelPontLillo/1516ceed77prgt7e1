package controlador;

import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import modelo.Alumno;
import modelo.Grupo;
import modelo.IModelo;
import vista.Funciones;
import vista.Verificador;
import vista.VistaGraficaAlumno;

/**
 * Fichero: ControladorAlumno.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/11/2016
 */

public class ControladorAlumno implements ActionListener, ItemListener {
    private IModelo modelo;
    private VistaGraficaAlumno vista;
    private String chivato = "";
    private ArrayList alumnos;
    private ArrayList grupos;
    private Alumno actual;
    private int indice;
    private Funciones f = new Funciones();
    
    public ControladorAlumno (IModelo m, VistaGraficaAlumno v) {
        modelo = m;
        vista = v;
        
        cancelar();
        cargarCombo();
       
        //Añadimos los listeners
        vista.getSalir().addActionListener(this);
        vista.getGuardar().addActionListener(this);
        vista.getCancelar().addActionListener(this);
        vista.getCreate().addActionListener(this);
        vista.getUpdate().addActionListener(this);
        vista.getRead().addActionListener(this);
        vista.getDelete().addActionListener(this);
        vista.getPrimero().addActionListener(this);
        vista.getAnterior().addActionListener(this);
        vista.getSiguiente().addActionListener(this);
        vista.getUltimo().addActionListener(this);
        vista.getCbGrupo().addItemListener(this);
    }
    
        //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getCreate()) {
            create();
        } else if (objeto == vista.getRead()) {
            read();
        } else if (objeto == vista.getUpdate()) {
            update();
        } else if (objeto == vista.getDelete()) {
            delete();
        } else if (objeto == vista.getPrimero()) {
            primero();
        } else if (objeto == vista.getAnterior()) {
            anterior();
        } else if (objeto == vista.getSiguiente()) {
            siguiente();
        } else if (objeto == vista.getUltimo()) {
            ultimo();
        } else if (objeto == vista.getGuardar()) {
            guardar(chivato);
        } else if (objeto == vista.getCancelar()) {
            cancelar();
        }
    }
    
    public void itemStateChanged(ItemEvent e) {
        Object objeto = e.getSource();
        
        if (objeto == vista.getCbGrupo()) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                cargaGrupo();
            }
        }
    }
    
    private void create() {
        chivato = "create";
        vista.getTid().setEnabled(true);
        vista.getTfecha().setEnabled(true);
        vista.getTnombre().setEnabled(true);
        vista.getTedad().setEnabled(true);
        vista.getTemail().setEnabled(true);
        vista.getTidGrupo().setEnabled(false);
        vista.getCbGrupo().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getRead().setEnabled(false);
        validaciones();
    }
    
    private void read() {
        chivato = "read";
        alumnos = modelo.readAlumno();
        vista.getPrimero().setEnabled(true);
        vista.getAnterior().setEnabled(true);
        vista.getSiguiente().setEnabled(true);
        vista.getUltimo().setEnabled(true);
        vista.getCreate().setEnabled(false);
        vista.getRead().setEnabled(false);
        vista.getUpdate().setEnabled(true);
        vista.getDelete().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        primero();
    }
    
    private void update() {
        chivato = "update";
        vista.getTnombre().setEnabled(true);
        vista.getTedad().setEnabled(true);
        vista.getTemail().setEnabled(true);
        vista.getTfecha().setEnabled(true);
        vista.getCbGrupo().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
        validaciones();
    }
    
    private void delete() { 
        Alumno alumno = alta();
        modelo.delete(alumno);
        cancelar();
    }   
    
    //Configuramos el comportamiento del menú de navegabilidad
    private void primero() {
        if (alumnos != null) {
            indice = 0;
            actual = ((Alumno)alumnos.get(indice));   
            vista.getTid().setText(actual.getId());
            vista.getTnombre().setText(actual.getNombre());
            vista.getTedad().setText(Integer.toString(actual.getEdad()));
            vista.getTemail().setText(actual.getEmail());
            vista.getTidGrupo().setText(actual.getGrupo().getId());
            vista.getTfecha().setDate(actual.getFecha());
            cargaGrupo();
        } else {
            actual = null;
            indice = -1;
        }
    }
        
    private void anterior()  {
        if (indice != 0)     {
            indice -= 1;
            actual = ((Alumno)alumnos.get(indice));
            vista.getTid().setText(actual.getId());
            vista.getTnombre().setText(actual.getNombre());
            vista.getTedad().setText(Integer.toString(actual.getEdad()));
            vista.getTemail().setText(actual.getEmail());
            vista.getTidGrupo().setText(actual.getGrupo().getId());
            vista.getTfecha().setDate(actual.getFecha());
            cargaGrupo();
        }
    }

    private void siguiente() {
        if (indice != alumnos.size() - 1) {
            indice += 1;
            actual = ((Alumno)alumnos.get(indice));
            vista.getTid().setText(actual.getId());
            vista.getTnombre().setText(actual.getNombre());
            vista.getTedad().setText(Integer.toString(actual.getEdad()));
            vista.getTemail().setText(actual.getEmail());
            vista.getTidGrupo().setText(actual.getGrupo().getId());
            vista.getTfecha().setDate(actual.getFecha());
            cargaGrupo();
        }
    }
  
    private void ultimo() {
        indice = (alumnos.size() - 1);
        actual = ((Alumno)alumnos.get(indice));
        vista.getTid().setText(actual.getId());
        vista.getTnombre().setText(actual.getNombre());
        vista.getTedad().setText(Integer.toString(actual.getEdad()));
        vista.getTemail().setText(actual.getEmail());
        vista.getTidGrupo().setText(actual.getGrupo().getId());
        vista.getTfecha().setDate(actual.getFecha());
        cargaGrupo();
    }
    
    //Configuramos el comportamiento de los botones guardar, cancelar y salir
    private void guardar(String chivato) {
        Alumno alumno;
        switch (chivato) {
            case "create":
                try {
                    alumno = alta();
                    modelo.create(alumno);
                    cancelar();
                } catch (Exception e) {
                    f.error("Ha ocurrido un error. Inténtalo de nuevo.");
                }
                break;
            case "update":
                try {
                    alumno = alta();
                    modelo.update(alumno);
                    cancelar();
                } catch (Exception e) {
                    f.error("Ha ocurrido un error. Inténtalo de nuevo.");
                }
                break;
        }
    }
    
    private void cancelar() {
        //Deshabilitamos campos de texto 
        vista.getTid().setEnabled(false);
        vista.getTnombre().setEnabled(false);
        vista.getTedad().setEnabled(false);
        vista.getTemail().setEnabled(false);
        vista.getTidGrupo().setEnabled(false);
        vista.getCbGrupo().setEnabled(false);
        vista.getTfecha().setEnabled(false);
        //Limpiamos campos de texto
        vista.getTid().setText("");
        vista.getTnombre().setText("");
        vista.getTedad().setText("");
        vista.getTemail().setText("");
        vista.getTidGrupo().setText("");
        vista.getCbGrupo().setSelectedItem(null);
        vista.getTfecha().setDate(null);
        //Configuramos guardar, cancelar
        vista.getGuardar().setEnabled(false);
        vista.getCancelar().setEnabled(false);
        //Configuramos CRUD
        vista.getCreate().setEnabled(true);
        vista.getRead().setEnabled(true);
        vista.getUpdate().setEnabled(false);
        vista.getDelete().setEnabled(false);
        //Configuramos navegabilidad
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
    }
    
    private void salir() {
        vista.dispose();
    }
    
    //Otras funciones
    private Alumno alta() {
        String id = vista.getTid().getText();
        String nombre = vista.getTnombre().getText();
        int edad = Integer.parseInt(vista.getTedad().getText());
        String email = vista.getTemail().getText();
        String idG = vista.getTidGrupo().getText();
        Grupo grupo = new Grupo();
        grupo.setId(idG);
        Date fecha = vista.getTfecha().getDate();
        Alumno alumno = new Alumno(id, nombre, edad, email, grupo, fecha);
        return alumno;
    }
    
    private void validaciones() {
        vista.getTedad().setInputVerifier(new Verificador("tedad"));
        vista.getTemail().setInputVerifier(new Verificador("temail"));
    }
    
    private void cargarCombo() {
        grupos = modelo.readGrupo();
        Iterator it = grupos.iterator(); 
        vista.getCbGrupo().addItem(null);
        while (it.hasNext()) { 
        Grupo grupo = (Grupo) it.next();
            vista.getCbGrupo().addItem(grupo.getId() + " - " + grupo.getNombre());
        }
    }    
    
    private void cargaGrupo() {
        String id;
        grupos = modelo.readGrupo();
        Iterator it;
        Grupo grupo;
        
        if (!vista.getTidGrupo().getText().equals("")) {
            id = vista.getTidGrupo().getText();
            it = grupos.iterator();
            while (it.hasNext()) { 
                grupo = (Grupo) it.next(); 
                if (grupo.getId().equals(id)) {
                    vista.getTidGrupo().setText(grupo.getId());
                    vista.getCbGrupo().setSelectedItem(grupo.getId() + " - " + grupo.getNombre());
                }
            }
        } else if (!vista.getCbGrupo().getSelectedItem().equals("")) {
            String combo = (String)vista.getCbGrupo().getSelectedItem();
            StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
            id = stokenizer.nextToken();
            grupos = modelo.readGrupo();
            it = grupos.iterator(); 
            while (it.hasNext()) { 
                grupo = (Grupo) it.next(); 
                if (grupo.getId().equals(id)) {
                    vista.getTidGrupo().setText(grupo.getId());
                }
            }
        }
    }
}