package controlador;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import modelo.IModelo;
import modelo.ModeloFichero;
import vista.VistaGrafica;
import vista.VistaGraficaAlumno;
//import vista.VistaGraficaGrupo;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.VistaGraficaAcerca;
import vista.VistaGraficaGrupo;

/**
 * Fichero: Controlador.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/11/2016
 */

public class Controlador implements ActionListener {
    private IModelo modelo = null;
    private VistaGrafica vista;
    
    public Controlador(IModelo m, VistaGrafica v) {
        modelo = m;
        vista = v;   
        
        //Añadimos los listeners
        vista.getAlumno().addActionListener(this);
        vista.getGrupo().addActionListener(this);
        vista.getDoc().addActionListener(this);
        vista.getAcerca().addActionListener(this);
        vista.getSalir().addActionListener(this);
    }
    
    //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getAlumno()) {
            try {
                alumno();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objeto == vista.getGrupo()) {
            try {
                grupo();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objeto == vista.getDoc()) {
            documentacion();
        } else if (objeto == vista.getAcerca()) {
            acerca();
        }
    }
    
    private void salir() {
        System.exit(0);
    }
    
    private void alumno() throws IOException {
        modelo = new ModeloFichero();
        VistaGraficaAlumno v = new VistaGraficaAlumno();
        ControladorAlumno ca = new ControladorAlumno(modelo, v);
    }
    
    private void grupo() throws IOException {
        modelo = new ModeloFichero();
        VistaGraficaGrupo v = new VistaGraficaGrupo();
        ControladorGrupo cg = new ControladorGrupo(modelo, v);    
    }
    
    private void documentacion() {
        String url = "https://docs.google.com/document/d/1pMHwoJj7_NjvooKJubFBl9eGE5t7wBHQi2BBPuGEUkg";
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.getDesktop().browse(new URI(url));
            } catch (URISyntaxException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void acerca() {
        VistaGraficaAcerca v = new VistaGraficaAcerca();
    }
}
