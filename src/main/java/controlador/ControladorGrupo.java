package controlador;

import java.awt.event.*;
import java.util.ArrayList;
import modelo.Grupo;
import modelo.IModelo;
import vista.Funciones;
import vista.VistaGraficaGrupo;

/**
 * Fichero: ControladorGrupo.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 13/03/2016
 */

public class ControladorGrupo implements ActionListener {
    private IModelo modelo;
    private VistaGraficaGrupo vista;
    private String chivato;
    private ArrayList grupos;
    private Grupo actual;
    private int indice;
    private Funciones f = new Funciones();
    
    public ControladorGrupo (IModelo m, VistaGraficaGrupo v) {
        modelo = m;
        vista = v;
        
        cancelar();
        
        //Añadimos los listeners
        vista.getSalir().addActionListener(this);
        vista.getGuardar().addActionListener(this);
        vista.getCancelar().addActionListener(this);
        vista.getCreate().addActionListener(this);
        vista.getUpdate().addActionListener(this);
        vista.getRead().addActionListener(this);
        vista.getDelete().addActionListener(this);
        vista.getPrimero().addActionListener(this);
        vista.getAnterior().addActionListener(this);
        vista.getSiguiente().addActionListener(this);
        vista.getUltimo().addActionListener(this);
    }
    
        //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getCreate()) {
            create();
        } else if (objeto == vista.getRead()) {
            read();
        } else if (objeto == vista.getUpdate()) {
            update();
        } else if (objeto == vista.getDelete()) {
            delete();
        } else if (objeto == vista.getPrimero()) {
            primero();
        } else if (objeto == vista.getAnterior()) {
            anterior();
        } else if (objeto == vista.getSiguiente()) {
            siguiente();
        } else if (objeto == vista.getUltimo()) {
            ultimo();
        } else if (objeto == vista.getGuardar()) {
            guardar(chivato);
        } else if (objeto == vista.getCancelar()) {
            cancelar();
        }
    }
    
    private void create() {
        chivato = "create";
        vista.getTidGrupo().setEnabled(true);
        vista.getTgrupo().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getRead().setEnabled(false);
    }
    
    private void read() {
        chivato = "read";
        grupos = modelo.readGrupo();
        vista.getPrimero().setEnabled(true);
        vista.getAnterior().setEnabled(true);
        vista.getSiguiente().setEnabled(true);
        vista.getUltimo().setEnabled(true);
        vista.getCreate().setEnabled(false);
        vista.getRead().setEnabled(false);
        vista.getUpdate().setEnabled(true);
        vista.getDelete().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        primero();
    }
    
    private void update() {
        chivato = "update";
        vista.getTidGrupo().setEnabled(false);
        vista.getTgrupo().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
    }
    
    private void delete() { 
        Grupo grupo = alta();
        modelo.delete(grupo);
        cancelar();
    }   
    
    //Configuramos el comportamiento del menú de navegabilidad
    private void primero() {
        if (grupos != null) {
            indice = 0;
            actual = ((Grupo)grupos.get(indice));   
            vista.getTidGrupo().setText(actual.getId());
            vista.getTgrupo().setText(actual.getNombre());
        } else {
            actual = null;
            indice = -1;
        }
    }
        
    private void anterior()  {
        if (indice != 0)     {
            indice -= 1;
            actual = ((Grupo)grupos.get(indice));  
            vista.getTidGrupo().setText(actual.getId());
            vista.getTgrupo().setText(actual.getNombre());
        }
    }

    private void siguiente() {
        if (indice != grupos.size() - 1) {
            indice += 1;
            actual = ((Grupo)grupos.get(indice));  
            vista.getTidGrupo().setText(actual.getId());
            vista.getTgrupo().setText(actual.getNombre());
        }
    }
  
    private void ultimo() {
        indice = (grupos.size() - 1);
        actual = ((Grupo)grupos.get(indice));  
        vista.getTidGrupo().setText(actual.getId());
        vista.getTgrupo().setText(actual.getNombre());
    }
    
    //Configuramos el comportamiento de los botones guardar, cancelar y salir
    private void guardar(String chivato) {
        Grupo grupo;
        switch (chivato) {
            case "create":
                try {
                    grupo = alta();
                    modelo.create(grupo);
                    cancelar();
                } catch (Exception e) {
                    f.error("Ha ocurrido un error. Inténtalo de nuevo.");
                }
                break;
            case "update":
                try {
                    grupo = alta();
                    modelo.update(grupo);
                    cancelar();
                } catch (Exception e) {
                    f.error("Ha ocurrido un error. Inténtalo de nuevo.");
                }
                break;
        }
    }
    
    private void cancelar() {
        //Deshabilitamos campos de texto 
        vista.getTidGrupo().setEnabled(false);
        vista.getTgrupo().setEnabled(false);
        //Limpiamos campos de texto
        vista.getTidGrupo().setText("");
        vista.getTgrupo().setText("");
        //Configuramos guardar, cancelar
        vista.getGuardar().setEnabled(false);
        vista.getCancelar().setEnabled(false);
        //Configuramos CRUD
        vista.getCreate().setEnabled(true);
        vista.getRead().setEnabled(true);
        vista.getUpdate().setEnabled(false);
        vista.getDelete().setEnabled(false);
        //Configuramos navegabilidad
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
    }
    
    private void salir() {
        vista.dispose();
    }
    
    //Otras funciones
    private Grupo alta() {
        Grupo grupo = new Grupo(vista.getTidGrupo().getText(), vista.getTgrupo().getText());
        return grupo;
    }
}